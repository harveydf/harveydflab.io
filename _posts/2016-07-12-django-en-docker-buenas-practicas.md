---
layout: post
comments: true
title:  'Django en docker: buenas prácticas'
date:   2016-07-12 13:00:00 +0100
categories: docker django
permalink: /django-en-docker-buenas-practicas/
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/_DJahG4FiOE" frameborder="0"  allowfullscreen></iframe>

En el post [Backup diario con docker y dropbox](/backup-diario-con-docker-y-dropbox-part-i/) hablamos de algunos de los **beneficios** de docker, como la facilidad de uso y el poder tener un ambiente de desarrollo en **minutos**.

Para este post quiero combinar dos cosas, cómo podemos **empezar a usar django en docker** y cómo podemos usar diferentes **entornos** (desarrollo, producción, pruebas, etc).

Según la buenas prácticas de django que podemos encontrar en el libro de **Two scoops of django** debemos tener diferentes tipos de archivos (settings y requirements) para cada entorno pero cómo podemos combinar esto con un proyecto en docker, pues a esa pregunta es la que vamos a responder.

## Docker
En la [documentación de docker](https://docs.docker.com/compose/django/) podemos encontrar un ***quickstart*** para tener un proyecto muy básico  ~~que prácticamente no sirve para nada~~ que sirve solamente para dar un entender muy superficialmente de qué va esto de django con docker. Pero obviamente **no clarifica** muchas dudas que van a ir surgiendo a medida que nuestro proyecto avanza.

Así que vamos a aprovechar ese *quickstart* para partir de ahí y construir nuestros ambientes usando las buenas prácticas de django.

Lo primero como siempre es crear una carpeta en dónde vamos a trabajar.

{% highlight console %}
$ mkdir -p ~/Tutoriales/django-environments && cd $_
{% endhighlight %}

Y para una mejor organización creamos una carpeta que vamos a llamar **webapp** que ahí es donde va a ir todo lo correspondiente a django.

{% highlight console %}
$ mkdir webapp && cd $_
{% endhighlight %}

Y ahí adentro (webapp) vamos a crear el primero de los archivos que nos dice el tutorial de docker llamado **Dockerfile**.

{% highlight docker %}
FROM python:3.5

ENV PYTHONUNBUFFERED=1
ENV WEBAPP_DIR=/webapp

RUN mkdir $WEBAPP_DIR

WORKDIR $WEBAPP_DIR

ADD requirements.txt $WEBAPP_DIR/

RUN pip install -r requirements.txt

ADD . $WEBAPP_DIR/
{% endhighlight %}

Después de esto creamos el segundo en esta misma carpeta, que si ya estamos familiarizados con django lo reconoceremos de inmediato, llamado **requirements.txt**

{% highlight text %}
django==1.9.6
psycopg2==2.6.1
{% endhighlight %}

Ahora en nuestra carpeta principal (django-environments) vamos a crear el segundo archivo que se llama **dockercompose.yml**

{% highlight yaml %}
version: "2"

services:
  db:
    image: postgres:9.5
    environment:
      - POSTGRES_DB=mydb
      - POSTGRES_USER=example
      - POSTGRES_PASSWORD=secret
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - db_data:/var/lib/postgresql/data/pgdata

  web:
    build: ./webapp
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - ./webapp:/webapp
    ports:
      - "8000:8000"
    depends_on:
      - db

volumes:
  db_data:
    external: true
{% endhighlight %}

Lo siguiente para hacer es crear el **proyecto** en django. 

{% highlight console %}
$ docker-compose run web django-admin.py startproject django_environments webapp/
{% endhighlight %}

Y por último conectar la base de datos, para eso editamos el archivo que está en `webapp/django_environments/settings.py`

{% highlight py3 %}
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mydb',
        'USER': 'example',
        'PASSWORD': 'secret',
        'HOST': 'db',
        'PORT': '5432',
    }
}
...
{% endhighlight %}

Con esto tenemos un muy básico proyecto de django con postgresql en docker. Ahora si lo importante, vamos a modificar los **settings** y **requirements** para seguir con las buenas prácticas

## Buenas Prácticas
En la introducción hablamos de que una buena práctica en django es usar **diferentes** entornos para cada tipo de situación. Por lo tanto el libro nos recomienda usar diferentes archivos para cada entorno. Por ejemplo la estructura de los **settings** se vería algo así:

{% highlight text %}
settings/
    __init__.py
    base.py
    dev.py (o local.py)
    staging.py
    test.py
    production.py
{% endhighlight %}

Como vemos cada archivo pertenece a un entorno, excepto base que sería la configuración común a todos y del cuál derivan los demás. Por ejemplo en **base.py** tendríamos las **INSTALLED_APPS** con todos los módulos de nuestro proyecto, pero en **dev.py** extenderíamos esa variable para agregar librerías como **django-debug-toolbar** o **django-extensions**.

Así que para efectos prácticos e ir avanzado ~~que se me duermen~~ vamos a crear la estructura de settings con base y dev.

Para eso creamos primero módulo de python (que consiste en una carpeta y un archivo \_\_init\_\_.py) llamado settings dentro de la ruta `webapp/django_environments`

{% highlight console %}
$ mkdir webapp/django_environments/settings
$ touch webapp/django_environments/settings/__init__.py
{% endhighlight %}

Listo ahora movemos nuestro archivo de **settings.py** a la carpeta de settings y lo renombramos como **base.py**

{% highlight console %}
$ cd webapp/django_environments
$ mv settings.py settings/base.py
{% endhighlight %}

Ahora dentro de la misma carpeta de settings creamos el archivo **dev.py**

{% highlight py3 %}
from .base import *

INSTALLED_APPS += [
	'django_extensions',
	'debug_toolbar',
] 
{% endhighlight %}

Ahora bien, esa configuración de la base de datos en **base.py** tiene un pequeño problema, primero esos datos varian según el entorno. En **desarrollo** nuestra BD puede tener un nombre diferente y un password sencillo de recordar, sin preocuparnos mucho por la seguridad. En cambio en **producción** queremos un password difícil de adivinar. Entonces vamos a cambiar en **base.py** la configuración de la base de datos siguiendo otra guía de las buenas prácticas de django y es usar variables de entorno.

{% highlight py3 %}
...
SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
    }
}
{% endhighlight %}

Noten que uso los mismos nombres que le pasamos al contenedor de postgres para reusarlos en un sólo archivo de **environment**. Ya llegaremos a ese archivo más adelante.

Tal cuál hicimos con los **settings** vamos a hacer con los **requirements**, la idea es tener una estructura muy parecida:

{% highlight text %}
requirements/
    base.txt
    dev.txt
    staging.txt
    production.txt
{% endhighlight %}

Creamos una carpeta llamada **requirements** dentro de **webapp/**, movemos el requirements.txt (que ya tenemos) dentro de esa carpeta y lo renombramos a **base.txt**

{% highlight console %}
$ mkdir requirements
$ mv requirements.txt requirements/base.txt
{% endhighlight %}

Ahora nuestro archivo de requirements correspondiente sólo al entorno de desarrollo que será **dev.txt**

{% highlight text %}
-r base.txt

django-debug-toolbar==1.4
django-extensions==1.6.7
{% endhighlight %}
Ya tenemos las estructuras y los archivos correspondientes a nuestro entorno de desarrollo pero faltan un par de cosas antes que podamos continuar con nuestro proyecto.

Necesitamos decirle a docker qué archivo de requirements tomar a la hora de crear la imagen y también necesitamos decirle a django cuál archivo de settings usar.

Si nos devolvemos al principio de este post cuando creamos el **Dockerfile** vemos que en la penúltima línea le decimos cuáles librerías de python son las que queremos instalar así que necesitamos cambiar ese archivo para que se adapte a nuestros entornos.

{% highlight docker %}
FROM python:3.5

ARG DJANGO_ENV

ENV PYTHONUNBUFFERED=1
ENV WEBAPP_DIR=/webapp

RUN mkdir $WEBAPP_DIR

WORKDIR $WEBAPP_DIR

ADD requirements/base.txt $WEBAPP_DIR/ 
ADD requirements/$DJANGO_ENV.txt $WEBAPP_DIR/

RUN pip install -r $DJANGO_ENV.txt

ADD . $WEBAPP_DIR/
{% endhighlight %}
Con la segunda línea esperamos un argumento (variable) llamada **DJANGO_ENV** y es usada como una variable de entorno.

Esa variable se la pasamos en el archivo de **docker-compose.yml**. Pero antes de modificar el docker-compose quiero que hagamos algo. Así como es bueno tener diferentes archivos de *settings* y de *requirments*, también es bueno tener **diferentes** archivos *docker-compose*. En este archivo va toda la configuración de nuestros contenedores y esa configuración igualmente varía **dependiendo** del entorno.

Por ejemplo, mientras en desarrollo tenemos expuesto el puerto **8000** (para acceder directamente desde nuestro navegador) y corremos el servidor de django con `manage.py runserver`, en producción no va a ser igual. En producción vamos a querer que se acceda por el puerto **80** y que alguien como **nginx** se encargue de la conexión a django. A demás vamos a levantar el servidor de django con algo como **gunicorn**.

Por lo tanto vamos a seguir la estructura de las buenas prácticas de django y las aplicamos a docker, entonces tendremos un archivo base y un archivo de desarrollo. Primero creamos el archivo **base.yml** (en la documentación de docker lo nombran como *common.yml*) en la raíz del proyecto

{% highlight yaml %}
version: "2"

services:
  db:
    image: postgres:9.5
    env_file: .env
    volumes:
      - db_data:/var/lib/postgresql/data/pgdata

  web:
    build:
      context: ./webapp
    env_file: .env
{% endhighlight %}

Ahora vamos a cambiar un poco el **docker-compose.yml** que ya teníamos

{% highlight yaml %}
version: "2"

services:
  db:
    extends:
      file: base.yml
      service: db    

  web:
    extends:
      file: base.yml
      service: web
    build:
      args:
        - DJANGO_ENV=dev
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - ./webapp:/webapp
    ports:
      - "8000:8000"
    depends_on:
      - db

volumes:
  db_data:
    external: true
{% endhighlight %}

**Nota**: Como bien lo dice la [documentación de docker](https://docs.docker.com/compose/extends/#extending-services) algunas secciones como **volumes** y **depends_on** se deben definir en cada compose final, no el en común.

Lo último que nos faltaría por definir sería el archivo con nuestras variables de entorno. Lo llamaremos **.env** (noten el . precedente en el archivo para hacerlo oculto). Este archivo **NO** deben incluirlo es su repositorio por obvias razones.

{% highlight text %}
POSTGRES_DB=mydb
POSTGRES_USER=example
POSTGRES_PASSWORD=secret
PGDATA=/var/lib/postgresql/data/pgdata
DJANGO_SETTINGS_MODULE=django_environments.settings.dev
DJANGO_SECRET_KEY='_!5ci4u1e+g-th_+)l0)9ep^n-_f+3knt%k_4yywee4^(a5(6^'
DB_HOST=db
DB_PORT=5432
{% endhighlight %}

Con esto tendríamos un entorno de desarrollo usando buenas prácticas de django y docker. Sólo resta hacer `docker-compose build` y `docker-compose up -d` para tener corriendo nuestro django de desarrollo.

Si queremos usar un archivo de producción con docker compose sería con `docker-compose -f production.yml build|up|run`.

**Nota**: el archivo de **docker-compose.yml** no le cambiamos el nombre a dev.yml o similares debido a que el archivo que busca por defecto el comando de **docker-compose** es *docker-compose.yml*. Si dejamos ese nombre no tenemos que hacer `docker-compose -f dev.yml` y siendo un entorno de desarrollo nos ahorra mucho tiempo ya que usamos bastante este comando.

Todo el código de este tutorial lo pueden conseguir en el [repositorio de github.](https://github.com/harveydf/tutoriales-django-environments)