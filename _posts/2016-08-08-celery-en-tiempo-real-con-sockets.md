---
layout: post
comments: true
title:  'Celery: en tiempo real con sockets'
date:   2016-08-08 13:00:00 +0100
categories: docker django celery sockets
permalink: /celery-en-tiempo-real-con-sockets/
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/cr0cbJ2PkJY" frameborder="0"  allowfullscreen></iframe>

Muchas veces, en nuestros proyectos, necesitamos ejecutar tareas **asincrónicas**, procesos que no bloqueen el hilo principal de procesamiento y que devuelva una **respuesta** pronta al cliente.

Algunas de estas tareas normalmente son enviar correos transaccionales, guardar logs sobre las visitas al sitio o ejecutar tareas periódicas entre otras.

Una **desventaja** de Django frente a otros frameworks como Node es que sus procesos son **sincrónicos**, es decir hasta que una tarea no termina su ejecución otra tarea tiene que esperar su turno en la cola de procesamiento antes de poder ejecutarse.

Usemos un **ejemplo** para entender un poco mejor. Si una persona quiere generar y descargar un reporte en archivo CSV, pero si el proceso se demora unos 3 minutos desde que se hace la petición hasta que se devuelve el archivo, **ninguna** otra petición de ninguna persona se podrá tener respuesta hasta que se libere la cola de procesamiento. En la vida real esto puede variar dependiendo del número de procesadores o núcleos que se tengan, el número de workers que se levanten, etc. Pero para hacerlo **simple** pensemos en el caso más simple, un solo procesador con un solo núcleo.

Aquí es dónde entran herramientas como **Celery** que nos permite ejecutar tareas de forma asincrónica y en segundo plano. Pero el problema de Celery es que una vez ejecuta la tarea no tiene cómo comunicarse con el cliente y decirle que ya se terminó la tarea. Para esto nos vamos a apoyar de **Node** y su poderoso **Socket.io**

### Empecemos
Para este ejemplo vamos a utilizar los siguientes servicios:

* Django
* Celery
* Node + Socket.io
* Redis
* Nginx

La idea es tener la siguiente arquitectura:

![arquitectura]({{ site.url }}/assets/img/posts/django-celery-realtime-jpeg.jpg)

Como siempre nos vamos a ayudar de Docker para no tener que instalar cada uno de estos servicios por aparte, ni entornos virtuales, ni variables de entorno para configurar nuestros servicios. Y para que sin importar el sistema operativo que tengamos sea igual para todos ;)

Empecemos con el **docker-compose.yml** que es nuestra configuración de servicios

{% highlight yaml %}
version: '2'

services:
  django:
    build: ./django
    volumes:
      - ./django:/webapp
    depends_on:
      - node
      - celery

  celery:
    build: ./django
    command: celery worker -A celery_realtime -l info
    volumes:
      - ./django:/webapp
    depends_on:
      - redis

  node:
    build: ./node
    volumes:
      - ./node:/nodeapp
    depends_on:
      - redis
    ports:
      - "3000:3000"

  nginx:
    image: nginx
    volumes:
      - ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro
    ports:
      - "80:80"
    depends_on:
      - django

  redis:
    image: redis

{% endhighlight %}


Ahora debemos crear cada una de las carpetas con sus **Dockefile** y demás archivos necesarios. El primero que vamos a hacer es el servicio de **Node** ya que es el más corto y fácil.

### Node
Node se encargará de escuchar de Redis cuando Celery diga que ya terminó y por medio de Socket.io le comunicará al cliente que ya terminó la tarea.

Lo primero será crear una carpeta llamada `node` y en ella crear un archivo llamado `Dockerfile`:
{% highlight docker %}
FROM node

ENV NODEDIR=/nodeapp

RUN mkdir $NODEDIR
WORKDIR $NODEDIR

COPY package.json $NODEDIR/
RUN npm install

COPY . $NODEDIR/

CMD ["npm", "start"]
{% endhighlight %}

Como vemos en la línea 8 del Dockerfile, necesitamos un archivo llamado `package.json` que vamos a crear dentro de la misma carpeta de `node`:
{% highlight js %}
{
  "name": "celery_realtime",
  "version": "1.0.0",
  "description": "Celery realtime con socket.io",
  "main": "server.js",
  "scripts": {
    "prestart": "npm install",
    "start": "node server.js"
  },
  "dependencies": {
    "jquery": "^3.1.0",
    "redis": "^2.6.2",
    "socket.io": "^1.4.5"
  }
}
{% endhighlight %}


Y según package.json necesitamos un archivo llamado `server.js` que es nuestro "servidor socket". Una vez más este archivo irá dentro de la carpeta de `node`:
{% highlight js %}
/* Creamos un servidor http y conectamos las
funciones de socket a este servidor */
var http = require('http').Server();
var io = require('socket.io')(http);
var redis = require('redis');

// Conectamos con el servidor de redis
var redisClient = redis.createClient(6379, 'redis');

/* Esta función recibe cualquier petición de
conexión desde un socket en el navegador (cliente)
e imprime en consola que un nuevo usuario se ha
conectado */
io.on('connection', function (socket) {
  console.log('A user has connected...');

  /* Cuando el socket cliente envíe un mensaje
  llamado 'subscribe', el socket se suscribe al
  canal de redis que tiene por nombre el ID de
  la tarea de Celery */
  socket.on('subscribe', function(celeryTaskId){
    redisClient.subscribe(celeryTaskId);
  })

  /* Cuando redis genere una publicación (message)
  el mesaje de esa publicación se emite al socket
  bajo el nombre de 'result' */
  redisClient.on('message', function(channel, message) {
    socket.emit('result', message);
  });

});

/* Levantamos el servidor http y lo ponemos a
escuchar en el puerto 3000 */
http.listen(3000, function (){
  console.log('Listening on port 3000...');
});
{% endhighlight %}

### Django
Ahora seguimos con el proyecto de django, igualmente debemos empezar creando una carpeta llamada `django` y dentro un archivo llamado `Dockerfile`:
{% highlight docker %}
FROM python:3.5

ENV PYTHONUNBUFFERED=1
ENV C_FORCE_ROOT=1
ENV DJANGODIR=/webapp

RUN mkdir $DJANGODIR
WORKDIR $DJANGODIR

COPY requirements.txt $DJANGODIR/
RUN pip install -r requirements.txt

COPY . $DJANGODIR/

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
{% endhighlight %}


Siguiendo el mismo patrón, necesitamos ahora un archivo llamado `requirements.txt` dentro de la carpeta de `django`:
{% highlight yaml %}
celery[redis]
django
redis
{% endhighlight %}

Ahora debemos crear nuestro proyecto y un módulo (*app*) en dónde vamos a tener la vista, el template y la tarea.

{% highlight console %}
$ docker-compose run django django-admin.py startproject celery_realtime .
$ docker-compose run django django-admin.py startapp realtime
{% endhighlight %}

Después debemos agregar nuestra *app* a los `INSTALLED_APPS` en `django/celery_realtime/settings.py`:
{% highlight python %}
...
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'realtime',
]
...
{% endhighlight %}

Creamos nuestra vista en el archivo `realtime/views.py`
{% highlight python %}
from django.http import HttpResponse
from django.views.generic import TemplateView

from .tasks import generate_csv

class CeleryRealTimeTemplateView(TemplateView):
    template_name = 'realtime.html'

    def post(self, request, *args, **kwargs):
        task_id = generate_csv.delay()
        return HttpResponse(task_id)
{% endhighlight %}

Dentro de la carpeta de `realtime` creamos una carpeta llamada `templates` y ahí dentro creamos el archivo `realtime.html`:
{% highlight html %}
{% raw %}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Celery Realtime</title>

  <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
  <h1>Celery Realtime</h1>

  <form class="" action="" method="post">
    {% csrf_token %}
    <input type="submit" name="" value="Exportar">
  </form>

  <div id="result"></div>

  <script>
    // Conectamos con el socket en el servidor de Node
    var socket = io('ws://localhost:3000');

    /* Cualquier mensaje que se reciba en el canal "result"
    se agrega dentro del div "result" */
    socket.on('result', function(csvURL) {
      var result = document.getElementById('result');
      var aTag = document.createElement('a');
      aTag.setAttribute('href', csvURL);
      aTag.setAttribute('target', '_blank');
      aTag.innerHTML = "Descargar archivo";
      result.appendChild(aTag);
    });

    /* Cuando se presione el botón de submit se envía
    una petición POST AJAX a la vista para ejecutar la tarea */
    var submit = $('input[type="submit"]');
    submit.on('click', function (event) {
      $.ajax({
        type: 'POST',
        url: "{% url 'realtime' %}",
        data: $('form').serialize(),
        success: function(celeryTaskId)
        {
          /* La vista devuelve el ID de la tarea de Celery
          y se lo envíamos al server para que sepa a cuál canal
          suscribirse */
          socket.emit('subscribe', celeryTaskId);
        }
      });

      event.preventDefault();
    });
  </script>
</body>
</html>
{% endraw %}
{% endhighlight %}

Enlazamos una url a la vista para podernos comunicar, por lo tanto en el archivo de `celery_realtime/urls.py` debería quedarnos así:
{% highlight python %}
from django.conf.urls import url
from django.contrib import admin

from realtime.views import CeleryRealTimeTemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^realtime/?$', CeleryRealTimeTemplateView.as_view(), name='realtime'),
]
{% endhighlight %}

### Celery
Este es el "servidor" de tareas que nos permitirá ejecutar tareas en segundo plano o asicrónicas.

Dentro de la carpeta de `realtime` creamos un archivo llamado `tasks.py`. Aquí es donde va a ir la tarea de Celery
{% highlight python %}
import time
import redis
from celery import task

redis_client = redis.StrictRedis(host='redis', port=6379, db=0)

@task(bind=True)
def generate_csv(self):
    time.sleep(10)
    redis_client.publish(self.request.id, 'http://bit.ly/2a2EiIQ')
{% endhighlight %}

La idea es con esta tarea emular que dura 10s (para el ejemplo le puse un delay bajo) y luego devuelve la url del archivo para descargarlo.

Para que celery nos funcione debemos configurarlo, para eso dentro de la carpeta `celery_realtime` donde está el archivo de `settings.py`, creamos un archivo llamado `celery.py`:
{% highlight python %}
import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_realtime.settings')

from django.conf import settings  # noqa

app = Celery('celery_realtime')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.conf.update(
    BROKER_URL = 'redis://redis:6379/0',
    CELERY_RESULT_BACKEND = 'redis://redis:6379/0',
)
{% endhighlight %}

Este archivo está basado en la documentación de [Celery](http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html#using-celery-with-django). Lo que a grandes rasgos hace es buscar un archivo `tasks` dentro de cada módulo de **INSTALLED_APPS** y registar en celery cada tarea que encuentre. Pero falta algo, Celery por si sólo no se "activa" cuando se levanta el servidor de django, hay que forzarlo a que encuentre todas las tareas. Para eso en el archivo de `celery_realtime/__init__.py` escribimos lo siguiente:
{% highlight python %}
from .celery import app as celery_app
{% endhighlight %}

### NGINX
Vamos a ayudarnos de NGINX para probar un ejemplo que es muy común, en el que el timeout de una respuesta es menor que lo que dura la respuesta. Es decir, digamos que NGINX tiene un timeout de 1 segundo, cualquier petición HTTP que dure más de un segundo NGINX la corta y devuelve un error. Pero ¿Si nuestra tarea de Celery dura 10 segundos eso quiere decir que no va a terminar? Si fuera una petición HTTP normal o una petición AJAX si, NGINX cortaría la respuesta y devolvería un error 504.

Pero cómo lo estamos haciendo con Celery y con socket, liberamos la respuesta al cliente "haciendo una promesa" de que estamos generando el archivo y una vez esté listo le avisamos, esa respuesta dura mucho menos de 1 segundo así que no tendremos problemas.

Entonces, creamos una carpeta llamada `nginx` a la misma par donde están las de `django` y `node`, y dentro creamos un archivo llamado `default.conf`:
{% highlight nginx %}
server {
	listen 80;
	server_name localhost;

	location / {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $http_host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_pass http://django:8000;
		proxy_read_timeout 1;
	}
}
{% endhighlight %}

Antes de probar todo recordemos hacer `docker build` al igual que crear la BD para django `docker-compose run django python manage.py migrate` sino va a dar error.

Todo el código creado en este post está en el [repositorio de gitlab](https://gitlab.com/harveydf-tutorials/django-celery-realtime/tree/master)